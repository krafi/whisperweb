from flask import Flask, request, jsonify
from flask_cors import CORS
import threading
import queue
import whisper
import os
from datetime import datetime
import argostranslate.package
import argostranslate.translate
app = Flask(__name__)
CORS(app)

current_model_name = "medium"
model = whisper.load_model(current_model_name)

file_queue = queue.Queue()

def load_whisper_model(model_name):
    global model
    model = whisper.load_model(model_name)

def process_file_worker():
    """Worker function to process audio files and store transcription results."""
    while True:
        filename, response_queue = file_queue.get()
        if filename is None:
            break
        try:
            print(f"Processing {filename}")
            result = model.transcribe(filename, language="ru", fp16=False, verbose=True)
            transcription = result['text']

            # Save transcription to a text file
            txt_filename = filename.replace(".wav", ".txt")
            with open(txt_filename, "w", encoding="utf-8") as txt_file:
                txt_file.write(transcription)

            response_queue.put(transcription)
        except Exception as e:
            print(f"Error processing file: {e}")
            response_queue.put(f"Error: {e}")
        finally:
            # Commenting out the deletion of the original audio file
            # os.remove(filename)
            file_queue.task_done()

worker_thread = threading.Thread(target=process_file_worker, daemon=True)
worker_thread.start()

@app.route('/ping', methods=['GET'])
def ping():
    """Check server status."""
    return "Pong", 200

@app.route('/upload', methods=['POST'])
def upload():
    """Upload an audio file for processing."""
    if 'file' not in request.files:
        return "No file part", 400
    file = request.files['file']
    if file.filename == '':
        return "No selected file", 400

    # Add current date and time to the filename
    current_time = datetime.now().strftime("%Y%m%d_%H%M%S")
    filename = f"uploads/{current_time}_{file.filename}"
    os.makedirs(os.path.dirname(filename), exist_ok=True)
    file.save(filename)

    response_queue = queue.Queue()
    file_queue.put((filename, response_queue))


    # Translation using Argos Translate
    from_code = "ru"
    to_code = "en"

    # Download and install Argos Translate package
    argostranslate.package.update_package_index()
    available_packages = argostranslate.package.get_available_packages()
    package_to_install = next(
        filter(
            lambda x: x.from_code == from_code and x.to_code == to_code, available_packages
        )
    )
    argostranslate.package.install_from_path(package_to_install.download())
    transcription_result = response_queue.get()
    print("transcription_result",transcription_result)
    translatedText = argostranslate.translate.translate(transcription_result, from_code, to_code)
    print("translatedText",translatedText)
    transcription_result_with_transelate =  transcription_result + "              /n          translate:" + translatedText 
    print("transcription_result_with_transelate",transcription_result_with_transelate)
    return jsonify({"transcription": transcription_result_with_transelate}), 200

@app.route('/set_model', methods=['POST'])
def set_model():
    """Set the Whisper model."""
    global current_model_name
    model_name = request.json.get('model_name')
    if model_name in ['tiny', 'base', 'small', 'medium']:
        current_model_name = model_name
        load_whisper_model(model_name)
        return jsonify({"status": "success", "message": f"Model changed to {model_name}"}), 200
    else:
        return jsonify({"status": "error", "message": "Invalid model name"}), 400

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000, ssl_context="adhoc")

