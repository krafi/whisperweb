# WhisperWeb
This project is a web-based audio recording and transcription service using OpenAI's Whisper model. The service allows users to record audio from their browser, send it to the server for transcription, and receive the transcribed text. Users can choose from different Whisper models (tiny, base, small, medium) for transcription.

UI demo: https://krafi.gitlab.io/whisperweb/ 
it will not work because it use https connection, you have to download index.html and run locally on client side
this comes with a weard issue , android system not allow microphone with local html file, 

but i figure out soul browser , you can find in playstore it handle this issue diffrently somehow it manage to use http even with this link https://krafi.gitlab.io/whisperweb/ 
## Features

- Record audio from the browser.
- Transcribe audio using OpenAI's Whisper model.
- Choose from different Whisper models for transcription.
- View and copy server responses.

## Setup

### Prerequisites for server

- Python 3.8+
- Flask
- Flask-CORS
- Whisper
- Argostranslate

### Installation

1. install necessary python package 
2. clone the repo
3. change directoy to the python file location 
4. run the server 
   ```bash
   pip install flask flask-cors whisper
   git clone https://gitlab.com/krafi/whisperweb.git
   cd whisperweb
   python server.py

 
    ```
5. you will see the server output you will see a line of you public ip address 
** Running on http://127.0.0.1:5000
 Running on http://112.13.76.219:5000** 
6. note in this case note my public ip address is 112.13.76.219 

7. Download the index.html and you can use open that index.html in your own local computer
8. set the ip address 112.13.76.219  and enjoy 

### Server Endpoints
GET /ping: Check server status.
POST /upload: Upload an audio file for transcription.
POST /set_model: Set the Whisper model (tiny, base, small, medium).

###Frontend
The frontend is a simple HTML page with JavaScript to handle audio recording, file uploading, and server interactions.

###Modifying the Whisper Model
You can select the Whisper model from the dropdown menu in the frontend. The available models are tiny, base, small, and medium. After selecting a model, click the "Set Model" button to apply the change.

###Contributing
Contributions are welcome! Please open an issue or submit a pull request.